import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IConference, Conference } from 'app/shared/model/conferences/conference.model';
import { ConferenceService } from './conference.service';

@Component({
  selector: 'jhi-conference-update',
  templateUrl: './conference-update.component.html'
})
export class ConferenceUpdateComponent implements OnInit {
  conference: IConference;
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    participants: [null, [Validators.required]],
    date: [null, [Validators.required]]
  });

  constructor(protected conferenceService: ConferenceService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ conference }) => {
      this.updateForm(conference);
      this.conference = conference;
    });
  }

  updateForm(conference: IConference) {
    this.editForm.patchValue({
      id: conference.id,
      name: conference.name,
      participants: conference.participants,
      date: conference.date != null ? conference.date.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const conference = this.createFromForm();
    if (conference.id !== undefined) {
      this.subscribeToSaveResponse(this.conferenceService.update(conference));
    } else {
      this.subscribeToSaveResponse(this.conferenceService.create(conference));
    }
  }

  private createFromForm(): IConference {
    const entity = {
      ...new Conference(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      participants: this.editForm.get(['participants']).value,
      date: this.editForm.get(['date']).value != null ? moment(this.editForm.get(['date']).value, DATE_TIME_FORMAT) : undefined
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IConference>>) {
    result.subscribe((res: HttpResponse<IConference>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
